package com.java.oop_homework;

import java.util.Locale;

public class ProcessorX86 extends Processor{

    private static final String architecture = "X86";
    public String getArchitecture(){
        return architecture;
    }
    @Override
    String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    String dataProcess(long data) {
        return String.valueOf(data);
    }
}
