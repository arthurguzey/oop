package com.java.oop_homework;

public class Memory {
    String[] memoryCell;

    public Memory(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }
    public Memory() {}

    public String readLast() {
        int i = memoryCell.length - 1;
        if(memoryCell[i] != null){
            return memoryCell[i];
        }
        return null;
    }

    public String removeLast(){
        String buffer = null;
        int i = memoryCell.length - 1;
        if(memoryCell[i] != null){
            memoryCell[i] = null;
            buffer = memoryCell[i];
            memoryCell[i] = null;
            return buffer;
        }
        return null;
    }

    public boolean save(String value){
        int i = memoryCell.length - 1;
        if(memoryCell[i] == null){
            memoryCell[i] = value;
            return true;
        }
        return false;
    }

    public MemoryInfo getMemoryInfo(){
        int memoryLength = memoryCell.length;
        int freeMemory = 0;
        for(int i =  0; i < memoryLength; i++){
            if(memoryCell[i] == null){
                freeMemory++;
            }
        }
        return new MemoryInfo(memoryLength, 100 * (memoryLength - freeMemory) /  memoryLength);
    }

}
