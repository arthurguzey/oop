package com.java.oop_homework;

public class ProcessorArm extends Processor {

    private static final String architecture  = "ARM";
    public String getArchitecture() {
        return architecture;
    }
    @Override
    String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    String dataProcess(long data) {
        return String.valueOf(data*10);
    }
}
