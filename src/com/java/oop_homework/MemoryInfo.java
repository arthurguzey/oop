package com.java.oop_homework;

public class MemoryInfo {
    private int memory;
    private int occupiedMemory;

    public int getMemory() {
        return memory;
    }

    public int getOccupiedMemory() {
        return occupiedMemory;
    }

    public MemoryInfo(int memory, int occupiedMemory) {
        this.memory = memory;
        this.occupiedMemory = occupiedMemory;
    }
}
