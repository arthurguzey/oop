package com.java.oop_homework;

public abstract class Processor {

    double frequency;
    double cache;
    int bitCapacity;

    public double getFrequency() {
        return frequency;
    }

    public double getCache() {
        return cache;
    }

    public int getBitCapacity() {
        return bitCapacity;
    }

    public String getDetails(){
        String details = "";
        details = "frequency = "+frequency+" cache = "+cache+" bitCapacity = "+bitCapacity;
        return details;
    }

    abstract String dataProcess(String data);
    abstract String dataProcess(long data);
}
