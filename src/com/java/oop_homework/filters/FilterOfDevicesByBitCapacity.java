package com.java.oop_homework.filters;

import com.java.oop_homework.Device;
import com.java.oop_homework.Processor;

import java.util.Comparator;

public class FilterOfDevicesByBitCapacity implements Comparator<Device> {
    @Override
    public int compare(Device o1, Device o2) {
        Processor p1 = o1.processor;
        Processor p2 = o2.processor;
        if(p1.getBitCapacity()> p2.getBitCapacity()){
            return 1;
        }
        else if(p1.getBitCapacity()> p2.getBitCapacity()){
            return -1;
        }
        return 0;
    }
}
