package com.java.oop_homework.filters;

import com.java.oop_homework.*;

import java.util.ArrayList;

public class FilterOfDevices {

    public ArrayList<Device> sortByArchitecture(ArrayList<Device> devices, String architecture){
        ArrayList<Device> buffer = new ArrayList<Device>();
        if(architecture.equals("ARM"))
        {
            for (int i=0; i<devices.size();i++)
            {
                if(devices.get(i).processor instanceof ProcessorArm)
                {
                    buffer.add(devices.get(i));
                }
            }
        }
        else if(architecture.equals("X86")){
            for (int i=0; i<devices.size();i++)
            {
                if(devices.get(i).processor instanceof ProcessorX86)
                {
                    buffer.add(devices.get(i));
                }
            }
        }
        return buffer;
    }

    public ArrayList<Device> sortByCache(ArrayList<Device> devices){
        FilterOfDevicesByCache filterOfDevicesByCache = new FilterOfDevicesByCache();
        devices.sort(filterOfDevicesByCache);
        return devices;
    }


    public ArrayList<Device> sortByFrequency(ArrayList<Device> devices){
        FilterOfDevicesByFrequency filterOfDevicesByFrequency = new FilterOfDevicesByFrequency();
        devices.sort(filterOfDevicesByFrequency);
        return devices;
    }

    public ArrayList<Device> sortByBitCapacity(ArrayList<Device> devices){
        FilterOfDevicesByBitCapacity filterOfDevicesByBitCapacity = new FilterOfDevicesByBitCapacity();
        devices.sort(filterOfDevicesByBitCapacity);
        return devices;
    }

    public ArrayList<Device> sortByMemoryMore(ArrayList<Device> devices, int memorySort){
        ArrayList<Device> buffer = new ArrayList<Device>();
        for (int i=0; i<devices.size();i++)
        {
            Memory memory = devices.get(i).memory;
            MemoryInfo memoryInfo = memory.getMemoryInfo();
            if(memoryInfo.getMemory()>memorySort)
            {
                buffer.add(devices.get(i));
            }
        }
        return devices;
    }

    public ArrayList<Device> sortByMemoryMLess(ArrayList<Device> devices, int memorySort){
        ArrayList<Device> buffer = new ArrayList<Device>();
        for (int i=0; i<devices.size();i++)
        {
            Memory memory = devices.get(i).memory;
            MemoryInfo memoryInfo = memory.getMemoryInfo();
            if(memoryInfo.getMemory()<memorySort)
            {
                buffer.add(devices.get(i));
            }
        }
        return devices;
    }


    public ArrayList<Device> sortByOccupiedMemoryMore(ArrayList<Device> devices, int memorySort){
        ArrayList<Device> buffer = new ArrayList<Device>();
        for (int i=0; i<devices.size();i++)
        {
            Memory memory = devices.get(i).memory;
            MemoryInfo memoryInfo = memory.getMemoryInfo();
            if(memoryInfo.getOccupiedMemory()>memorySort)
            {
                buffer.add(devices.get(i));
            }
        }
        return devices;
    }

    public ArrayList<Device> sortByOccupiedMemoryLess(ArrayList<Device> devices, int memorySort){
        ArrayList<Device> buffer = new ArrayList<Device>();
        for (int i=0; i<devices.size();i++)
        {
            Memory memory = devices.get(i).memory;
            MemoryInfo memoryInfo = memory.getMemoryInfo();
            if(memoryInfo.getOccupiedMemory()<memorySort)
            {
                buffer.add(devices.get(i));
            }
        }
        return devices;
    }


}
